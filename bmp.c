#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include "bmp.h"

char *shiftLeft(int n);


char *shiftLeft(int n) {
    char string[] = {"ABCDEFGHIJKLMNOPQRSTUVWXYZ"};
    char *moved = calloc(strlen(string)+1, sizeof(char));
    const int num_letters = 26;
    int i = 0;
    for (i = 0; i < strlen(string); i++) {
        if (string[i] >= 'a' && string[i] <= 'z') {
            string[i] = 'a' + (string[i] - 'a' + n) % num_letters;
        }
        else if (string[i] >= 'A' && string[i] <= 'Z') {
            string[i] = 'A' + (string[i] - 'A' + n) % num_letters;
        }
    }
    strcpy(moved, string);
    return moved;
}

char *reverse(const char *text) {
    if(text == NULL) return NULL;
    
    char *copy = calloc(strlen(text)+1, sizeof(char));
    strcpy(copy, text);
    char *begin, *end, temp;
    int len = strlen(copy), idx = 0;
    
    begin  = copy;
    end    = copy;
 
    for (idx = 0; idx < len-1; idx++) end++;
 
    for (idx = 0; idx < len/2; idx++) {        
        temp   = *end;
        *end   = *begin;
        *begin = temp;
        begin++;
        end--;
    } idx = 0;

    while(copy[idx] != '\0') {
        copy[idx] = toupper(copy[idx]);
        idx++;
    }
    return copy;
}

char *vigenere_encrypt(const char *key, const char *text) {
    if(text == NULL) return NULL;
    if(key == NULL) return NULL;
    if(key[0] == '\0') return NULL;
    for(int i = 0; i < strlen(key); i++) {
        if(isalpha(key[i]) == 0) return NULL;
        if(isalpha(key[i]) == ' ') return NULL;
    }

    char *encrypted = calloc(strlen(text)+1, sizeof(char));
    char alphabet[26] = {"ABCDEFGHIJKLMNOPQRSTUVWXYZ"};
    char text_copy[strlen(text)+1];
    char key_copy[strlen(key)+1];
    int idx = 0, text_len = strlen(text);

    strcpy(text_copy, text);
    text_copy[strlen(text_copy)+1] = '\0';
    strcpy(key_copy, key);
    key_copy[strlen(key_copy)+1] = '\0';

    while(text_copy[idx] != '\0') {
        text_copy[idx] = toupper(text_copy[idx]);
        idx++;
    } idx = 0;

    while(key_copy[idx] != '\0') {
        key_copy[idx] = toupper(key_copy[idx]);
        idx++;
    } idx = 0;

    int col = 0;
    for(size_t text_index = 0, key_index = 0 ; text_index < text_len; text_index++, key_index++){
        if(key_index == strlen(key_copy)) key_index = 0;
        for(size_t alpha_idx = 0; alpha_idx < 26; alpha_idx++) {
            if(text_copy[text_index] == alphabet[alpha_idx]) col = alpha_idx;
        }
        if(isalpha(text_copy[text_index]) == 0) {
            encrypted[text_index] = text_copy[text_index];
            key_index--;
            continue;
        }
        int n = key_copy[key_index] - 65;
        char *shifted = shiftLeft(n);
        encrypted[text_index] = shifted[col];
        free(shifted);
    }
    return encrypted;
}

char *vigenere_decrypt(const char *key, const char *text) {
    if(text == NULL) return NULL;
    if(key == NULL) return NULL;
    if(key[0] == '\0') return NULL;
    for(int i = 0; i < strlen(key); i++) {
        if(isalpha(key[i]) == 0) return NULL;
        if(isalpha(key[i]) == ' ') return NULL;
    }


    char *decrypted = calloc(strlen(text)+1, sizeof(char));
    char alphabet[26] = {"ABCDEFGHIJKLMNOPQRSTUVWXYZ"};
    char text_copy[strlen(text)+1];
    char key_copy[strlen(key)+1];
    int idx = 0, text_len = strlen(text);

    strcpy(text_copy, text);
    text_copy[strlen(text_copy)+1] = '\0';
    strcpy(key_copy, key);
    key_copy[strlen(key_copy)+1] = '\0';

    while(text_copy[idx] != '\0') {
        text_copy[idx] = toupper(text_copy[idx]);
        idx++;
    } idx = 0;

    while(key_copy[idx] != '\0') {
        key_copy[idx] = toupper(key_copy[idx]);
        idx++;
    } idx = 0;

    int col = 0;
    for(size_t text_index = 0, key_index = 0 ; text_index < text_len; text_index++, key_index++){
        if(key_index == strlen(key_copy)) key_index = 0;
        int n = key_copy[key_index] - 65;
        char *shifted = shiftLeft(n);
        for(size_t shifted_idx = 0; shifted_idx < 26; shifted_idx++) {
            if(text_copy[text_index] == shifted[shifted_idx]) col = shifted_idx;
        }
        free(shifted);
        if(isalpha(text_copy[text_index]) == 0) {
            decrypted[text_index] = text_copy[text_index];
            key_index--;
            continue;
        }
        decrypted[text_index] = alphabet[col];
    }
    return decrypted;
}


unsigned char *bit_encrypt(const char *text) {
    if(text == NULL) return NULL;

    unsigned char *encrypted = calloc(strlen(text)+1, sizeof(char));
    int length = strlen(text);
    int idx = 0, idx2 = 0;

    for(size_t index = 0; index < length; index++) {
        int binary[] = {0,0,0,0,0,0,0,0};
        unsigned int letter;
        
        //converting to binary
        letter = text[index];
        idx = 7;
        while(letter > 0) {
            binary[idx] = letter % 2;
            letter = letter / 2;
            idx--;
        } idx = 0;
        
        //switching couples in first half
        int tmp = 0;
        for(idx = 0; idx < 3; idx += 2) {
            tmp = binary[idx];
            binary[idx] = binary[idx+1];
            binary[idx+1] = tmp;
        } idx = 0; tmp = 0;

        //doing XOR
        for(idx = 4, idx2 = 0; idx <= 7; idx++, idx2++) {
            if(binary[idx] == binary[idx2]) binary[idx] = 0;
            else binary[idx] = 1;
        } idx = 0;

        //converting back to char
        tmp = 0;
        for(idx = 0, idx2 = 7; idx < 8; idx++, idx2--) {
            tmp = tmp + binary[idx] * pow(2,idx2);
        }
        encrypted[index] = tmp;
    }
    printf("\n");
    return encrypted;
}

char *bit_decrypt(const unsigned char *text) {
    if(text == NULL) return NULL;

    char *decrypted = calloc(strlen((char*)text)+1, sizeof(char));
    int length = strlen((char*) text);
    int idx = 0, idx2 = 0;

    for(size_t index = 0; index < length; index++) {
        int binary[] = {0,0,0,0,0,0,0,0};
        unsigned int letter;
        
        //converting to binary
        letter = text[index];
        idx = 7;
        while(letter > 0) {
            binary[idx] = letter % 2;
            letter = letter / 2;
            idx--;
        } idx = 0;
        
         //doing XOR
        for(idx = 4, idx2 = 0; idx <= 7; idx++, idx2++) {
            if(binary[idx] == binary[idx2]) binary[idx] = 0;
            else binary[idx] = 1;
        } idx = 0;
        
        //switching couples in first half
        int tmp = 0;
        for(idx = 0; idx < 3; idx += 2) {
            tmp = binary[idx];
            binary[idx] = binary[idx+1];
            binary[idx+1] = tmp;
        } idx = 0; tmp = 0;

        //converting back to char
        tmp = 0;
        for(idx = 0, idx2 = 7; idx < 8; idx++, idx2--) {
            tmp = tmp + binary[idx] * pow(2,idx2);
        }
        decrypted[index] = tmp;
    }
    printf("\n");
    return decrypted;
}


unsigned char *bmp_encrypt(const char *key, const char *text) {
    if(key[0] == '\0') return NULL;
    if(text[0] == '\0') return NULL;
    if(text == NULL) return NULL;
    if(key == NULL) return NULL;
    for(int i = 0; i < strlen(key); i++) {
        if(isalpha(key[i]) == 0) return NULL;
        if(isalpha(key[i]) == ' ') return NULL;
    }


    char *encrypted1;
    char *encrypted2;
    unsigned char *encrypted3;
    //unsigned char * encrypted = calloc(strlen(encrypted2)+1, sizeof(char));

    encrypted1 = reverse(text);
    encrypted2 = vigenere_encrypt(key, encrypted1);
    encrypted3 = bit_encrypt(encrypted2);
    unsigned char * encrypted = calloc(strlen((char*)encrypted3)+1, sizeof(char));

    strcpy((char*) encrypted, (const char *) encrypted3);

    free(encrypted1);
    free(encrypted2);
    free(encrypted3);

    return encrypted;
}

char* bmp_decrypt(const char* key, const unsigned char* text){
    if(text == NULL) return NULL;
    if(text[0] == '\0') return NULL;
    if(key[0] == '\0') return NULL;
    if(key == NULL) return NULL;
    for(int i = 0; i < strlen(key); i++) {
        if(isalpha(key[i]) == 0) return NULL;
        if(isalpha(key[i]) == ' ') return NULL;
    }


    char *decrypted1;
    char *decrypted2;
    char *decrypted3;
    //char * decrypted = calloc(strlen(decrypted2)+1, sizeof(char));

    decrypted1 = bit_decrypt(text);
    decrypted2 = vigenere_decrypt(key, decrypted1);
    decrypted3 = reverse(decrypted2);
    char * decrypted = calloc(strlen(decrypted3)+1, sizeof(char));


    strcpy(decrypted, decrypted3);

    free(decrypted1);
    free(decrypted2);
    free(decrypted3);

    return decrypted;

}






