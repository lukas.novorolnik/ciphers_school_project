#include <stdio.h>
#include <stdlib.h>
#include "greatest.h"
#include "../playfair.h"

TEST playfair_encrypt_with_even_length_of_string() {
    char *playfair = playfair_encrypt("SeCret", "Hello World");

    ASSERT_STR_EQ("ISJZJQXNTKJC", playfair);

    free(playfair);
    PASS();
}

TEST playfair_encrypt_with_odd_length_of_string() {
    char *playfair = playfair_encrypt("World", "Hello");

    ASSERT_STR_EQ("JBRYDR", playfair);

    free(playfair);
    PASS();
}

TEST playfair_encrypt_with_X_in_the_string() {
    char *playfair = playfair_encrypt("Password", "taxi please");
    
    ASSERT_STR_EQ("ARYHSJFPPH", playfair);

    free(playfair);
    PASS();
}

TEST playfair_encrypt_with_multiple_X() {
    char *playfair = playfair_encrypt("please", "taxxxiii");

    ASSERT_STR_EQ("XPADAJVJJV", playfair);

    free(playfair);
    PASS();
}

TEST playfair_with_wrong_input_params() {
    char *text = '\0';
    char *playfair = playfair_encrypt("please", text);

    ASSERT_EQ(NULL, playfair);
    free(playfair);
    
    char *key = "!3=1";
    playfair = playfair_encrypt(key, "text");
    ASSERT_EQ(NULL, playfair);
    free(playfair);

    PASS();
}

SUITE(playfair_tests) {
    RUN_TEST(playfair_encrypt_with_even_length_of_string);
    RUN_TEST(playfair_encrypt_with_odd_length_of_string);
    RUN_TEST(playfair_encrypt_with_X_in_the_string);
    RUN_TEST(playfair_encrypt_with_multiple_X);
    RUN_TEST(playfair_with_wrong_input_params);
}