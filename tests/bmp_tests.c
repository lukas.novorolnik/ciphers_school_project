#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "greatest.h"
#include "../bmp.h"

TEST reverse_encrypt() {
    char *text = "hello world";
    char *reversed = reverse(text);

    ASSERT_STR_EQ("DLROW OLLEH", reversed);
    ASSERT_EQ(strlen(reversed), strlen(text));

    free(reversed);
    PASS();
}

TEST vigenere_with_wrong_parameters() {
    const char *key = "k3y";
    char *vigenere = vigenere_encrypt(key, "computer");

    ASSERT_EQ(NULL, vigenere);
    free(vigenere);
    
    char *text = '\0';
    vigenere = vigenere_encrypt("key", text);
    ASSERT_EQ(NULL, vigenere);
    free(vigenere);

    PASS();
}

TEST vigenere_encrypt_basic_test() {
    char *vigenere = vigenere_encrypt("ComPuter", "HeLlo WorlD");

    ASSERT_STR_EQ("JSXAI PSINR", vigenere);

    free(vigenere);
    PASS();
}

SUITE(bmp_tests) {
    RUN_TEST(reverse_encrypt);
    RUN_TEST(vigenere_with_wrong_parameters);
    RUN_TEST(vigenere_encrypt_basic_test);
}


