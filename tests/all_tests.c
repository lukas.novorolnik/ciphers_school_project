#include "greatest.h"

extern SUITE(playfair_tests);
extern SUITE(bmp_tests);

GREATEST_MAIN_DEFS();

int main(int argc, char **argv) {
    GREATEST_MAIN_BEGIN();
    RUN_SUITE(playfair_tests);
    RUN_SUITE(bmp_tests);
    GREATEST_MAIN_END();
}
