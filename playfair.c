#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "playfair.h"

#define MX 5

void playfair(char *buffer, char key[MX][MX]);


void playfair(char *buffer, char key[MX][MX]) {  
    int row, col, row_idx_ch1, col_idx_ch1, row_idx_ch2, col_idx_ch2;
    for (row = 0; row < MX; row++) {  
        for (col = 0; col < MX; col++) {  
            if (buffer[0] == key[row][col]) {  
                row_idx_ch1 = row;  
                col_idx_ch1 = col;  
            } else if (buffer[1] == key[row][col]) {  
                row_idx_ch2 = row;  
                col_idx_ch2 = col;  
            }  
        }  
    }  

    if (row_idx_ch1 == row_idx_ch2) {  
        col_idx_ch1 = (col_idx_ch1 + 1) % 5;  
        col_idx_ch2 = (col_idx_ch2 + 1) % 5;  
        buffer[0] = key[row_idx_ch1][col_idx_ch1];
        buffer[1] = key[row_idx_ch2][col_idx_ch2];    
    } else if (col_idx_ch1 == col_idx_ch2) {  
        row_idx_ch1 = (row_idx_ch1 + 1) % 5;  
        row_idx_ch2 = (row_idx_ch2 + 1) % 5;  
        buffer[0] = key[row_idx_ch1][col_idx_ch1];
        buffer[1] = key[row_idx_ch2][col_idx_ch2];  
    } else {  
        buffer[0] = key[row_idx_ch1][col_idx_ch2];
        buffer[1] = key[row_idx_ch2][col_idx_ch1];  
    }  
} 

void playfair_reverse(char *buffer, char key[MX][MX]) {  
    int row, col, row_idx_ch1, col_idx_ch1, row_idx_ch2, col_idx_ch2;
    for (row = 0; row < MX; row++) {  
        for (col = 0; col < MX; col++) {  
            if (buffer[0] == key[row][col]) {  
                row_idx_ch1 = row;  
                col_idx_ch1 = col;  
            } else if (buffer[1] == key[row][col]) {  
                row_idx_ch2 = row;  
                col_idx_ch2 = col;  
            }  
        }  
    }  
    //printf("%d %d %d %d\n",w,x,y,z);  
    if (row_idx_ch1 == row_idx_ch2) {  
        col_idx_ch1 = (col_idx_ch1 - 1) % 5;  
        col_idx_ch2 = (col_idx_ch2 - 1) % 5;  
        buffer[0] = key[row_idx_ch1][col_idx_ch1];
        buffer[1] = key[row_idx_ch2][col_idx_ch2];    
    } else if (col_idx_ch1 == col_idx_ch2) {  
        row_idx_ch1 = (row_idx_ch1 - 1) % 5;  
        row_idx_ch2 = (row_idx_ch2 - 1) % 5;  
        buffer[0] = key[row_idx_ch1][col_idx_ch1];
        buffer[1] = key[row_idx_ch2][col_idx_ch2];  
    } else {  
        buffer[0] = key[row_idx_ch1][col_idx_ch2];
        buffer[1] = key[row_idx_ch2][col_idx_ch1];  
    }  
}   

char *playfair_encrypt(const char *key, const char *text) { 
    if(key == NULL) return NULL;
    if(text == NULL) return NULL;

    char *encrypted = calloc(strlen(text)+1, sizeof(char));
    char key_copy[strlen(key)+1]; 
    //char text_copy[strlen(text)+1];
    int k = 0, m = 0, key_length =0, text_length = 0, counter = 0;
    char buffer[2];
    char key_matrix[MX][MX], keyminus[25];  

    strcpy(key_copy, key);
    //strcpy(text_copy, text);
      
    key_length = strlen(key); 

    //convert the characters of key to uppertext  
    for (size_t idx = 0; idx < key_length; idx++) {  
        key_copy[idx] = toupper(key_copy[idx]);
        if(key_copy[idx] == 'W') key_copy[idx] = 'V';
    } 

    // store all characters except key
    int minus_idx = 0;   
    for (size_t idx = 0; idx < 26; idx++) {  
        for (k = 0; k < key_length; k++) {  
            if (key_copy[k] == ALPHA[idx]) break;  
        }  
        if (k == key_length) {  
            keyminus[minus_idx] = ALPHA[idx];  
            minus_idx++;  
        }  
    }  
    //construct key keymatrix  
    k = 0;  
    for (size_t i = 0; i < MX; i++) {  
        for (size_t j = 0; j < MX; j++) {  
            if (k < key_length) {  
                key_matrix[i][j] = key_copy[k];  
                k++;  
            } else {  
                key_matrix[i][j] = keyminus[m];  
                m++;  
            }  
            //printf("%c ", key_matrix[i][j]);  
        }  
        //printf("\n");  
    }  

    if((strlen(text)%2) == 0) text_length = strlen(text);
    else text_length = strlen(text)+1;
    for(int i = 0; i < text_length; i += 2) {
        buffer[0] = text[i];
        buffer[1] = text[i+1];
        if(buffer[0] == ' ') counter--;
        if(buffer[1] == ' ') counter--;
        //printf("BUFFER 0: %c\nBUFFER 1: %c\n", buffer[0], buffer[1]);
        if((buffer[0] != 'x' || buffer[0] != 'X') && buffer[0] == buffer[1]) counter++;
        //printf("counter: %d\n", counter);
    }

    //creating array for digraphs
    if((strlen(text+counter)%2) == 0) text_length = strlen(text)+counter;
    else text_length = strlen(text)+counter+1;
    char digraphs[text_length/2][2];

    //filling digraphs array with text
    for(size_t line = 0, idx_text = 0; line < text_length/2; line++, idx_text+=2) {
        buffer[0] = text[idx_text];
        buffer[1] = text[idx_text+1];
        if(buffer[0] == ' ' || buffer[1] == ' ') idx_text++;
        if((buffer[0] != 'x' || buffer [0] != 'X') && buffer[0] == buffer[1]) {
            digraphs[line][0] = text[idx_text];
            digraphs[line][0] = toupper(digraphs[line][0]);
            digraphs[line][1] = 'X';
            idx_text--;
        } else {
            digraphs[line][0] = text[idx_text];
            digraphs[line][0] = toupper(digraphs[line][0]);
            digraphs[line][1] = text[idx_text+1];
            digraphs[line][1] = toupper(digraphs[line][1]);
        }
        if(digraphs[line][1] == '\0') digraphs[line][1] = 'X';


    }
    /*
    //printing text
    for(size_t row = 0; row < text_length/2; ++row) {
        for(size_t col = 0; col < 2; col++) {
            printf("%c", digraphs[row][col]);
        }
        printf("\n");
    }   */

    // construct diagram and convert to cipher text  
    //printf("\nEntered text: %s\n", text);  
    for(size_t line = 0; line < text_length/2; line++) {
        playfair(digraphs[line], key_matrix);
    }
    
    //asigninng to encrypted
    for(size_t row = 0, idx_encrypted = 0; row < text_length/2; row++) {
        for(size_t col = 0; col < 2; col++) {
            encrypted[idx_encrypted] = digraphs[row][col];
            idx_encrypted++;
        }
    }   
    return encrypted;
}  

char* playfair_decrypt(const char* key, const char* text) {
 char *decrypted = calloc(strlen(text)+1, sizeof(char));
    char key_copy[strlen(key)+1]; 
    char text_copy[strlen(text)+1];
    int k = 0, m = 0, key_length =0, text_length = 0;
    char buffer[2];
    char key_matrix[MX][MX], keyminus[25];  

    strcpy(key_copy, key);
    strcpy(text_copy, text);
      
    key_length = strlen(key); 

    //convert the characters of key to uppertext  
    for (size_t idx = 0; idx < key_length; idx++) {  
        key_copy[idx] = toupper(key_copy[idx]);
        if(key_copy[idx] == 'W') key_copy[idx] = 'V';
    } 

    // store all characters except key
    int minus_idx = 0;   
    for (size_t idx = 0; idx < 26; idx++) {  
        for (k = 0; k < key_length; k++) {  
            if (key_copy[k] == ALPHA[idx]) break;  
        }  
        if (k == key_length) {  
            keyminus[minus_idx] = ALPHA[idx];  
            minus_idx++;  
        }  
    }  

    //construct key keymatrix  
    k = 0;  
    for (size_t i = 0; i < MX; i++) {  
        for (size_t j = 0; j < MX; j++) {  
            if (k < key_length) {  
                key_matrix[i][j] = key_copy[k];  
                k++;  
            } else {  
                key_matrix[i][j] = keyminus[m];  
                m++;  
            }  
            //printf("%c ", key_matrix[i][j]);  
        }  
        //printf("\n");  
    }  
   
    text_length = strlen(text);
    
    // converting cypher to text  
    //printf("\n\nEntered text :%s\n", text);  
    for(size_t idx = 0; idx < text_length; idx+=2) {
        buffer[0] = text_copy[idx];
        buffer[1] = text_copy[idx+1];
        playfair_reverse(buffer, key_matrix);
        decrypted[idx] = buffer[0];
        decrypted[idx+1] = buffer[1];
    }

    return decrypted;
}  


