#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "playfair.h"
#include "bmp.h"


int main() {
    char *encrypted;
    encrypted = playfair_encrypt("please", "taxxxiii");
    printf("Encrypted: %s\n", encrypted);
    printf("\n****************\n\n");
    char *decrypted;
    decrypted = playfair_decrypt("world", encrypted);
    printf("Decrypted: %s\n", decrypted);
    free(encrypted);
    free(decrypted);
    
    char *reversed = reverse("hello world");
    printf("%s", reversed);
    printf("\n");


    
    char *vigenere = vigenere_encrypt("Computer", "Hello World");
    printf("%s\n", vigenere);
    free(encrypted);
    /*
    char *decrypted = vigenere_decrypt("Computer", "JSXAI PSINR");
    printf("%s\n\n", decrypted);
    free(decrypted);
    */

    unsigned char *bit = bit_encrypt("Hello World!");
    //printf("%s", bit);
    for(int i = 0; i < strlen((char*) bit); i++) {
        printf("%x ", bit[i]);
    }
    free(bit);
    printf("\n");

    /* 
    char *bit_decrypted = bit_decrypt(bit);
    printf("\n%s", bit_decrypted);
    printf("\n");
    free(bit);
    free(bit_decrypted);
    */
    
    
    return 0;
}
