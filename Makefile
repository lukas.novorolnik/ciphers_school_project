CC = gcc
CFLAGS = -std=c11 -Wall -Werror
LDLIBS = -lm
OUTPUT = main
MODULES = playfair.o bmp.o
TESTS = tests/all_tests
TESTS_MODULES = tests/playfair_tests.o tests/bmp_tests.o

# Main tasks
.PHONY: all test clean

all: $(OUTPUT) $(TESTS)

test: $(TESTS)
	$<

clean:
	rm *.o tests/*.o $(OUTPUT) $(TESTS)

#Dependecies
$(OUTPUT): main.o $(MODULES)
main.o: main.c playfair.h bmp.h
playfair.o: playfair.c playfair.h
bmp.o: bmp.c bmp.h

$(TESTS): tests/all_tests.o $(TESTS_MODULES) $(MODULES)
tests/playfair_tests.o: tests/playfair_tests.c playfair.h
tests/bmp_tests.o: tests/bmp_tests.c bmp.h

# Machinery
%.o: %.c
	$(CC) -c -o $@ $(CFLAGS) $<
%: %.o
	$(CC) -o $@ $^ $(LDLIBS)

